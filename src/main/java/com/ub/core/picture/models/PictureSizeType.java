package com.ub.core.picture.models;

/**
 * Created by maslov on 20.12.16.
 */
public enum PictureSizeType {
    IMAGE_MAGIC, SCALR
}
