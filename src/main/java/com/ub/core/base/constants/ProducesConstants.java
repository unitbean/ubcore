package com.ub.core.base.constants;

public class ProducesConstants {
    public static final String APP_JSON_UTF8 = "application/json; charset=utf-8";
    public static final String TEXT = "text/plain; charset=utf-8";
}
