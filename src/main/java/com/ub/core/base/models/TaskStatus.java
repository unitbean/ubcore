package com.ub.core.base.models;

public enum TaskStatus {
    NEW, IN_PROGRESS, SUCCESS, ERROR
}
