package com.ub.core.pictureTask.menu;

import com.ub.core.base.menu.CoreMenu;
import com.ub.core.menu.models.fields.MenuIcons;

public class PictureTaskMenu extends CoreMenu {
    public PictureTaskMenu() {
        this.name = "Picture Tasks";
        this.icon = MenuIcons.ENTYPO_DOC_TEXT;
    }
}
