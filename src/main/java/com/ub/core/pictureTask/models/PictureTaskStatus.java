package com.ub.core.pictureTask.models;

/**
 * Created by maslov on 20.12.16.
 */
public enum PictureTaskStatus {
    NEW, ERROR, SUCCESS;
}
